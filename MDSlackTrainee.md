# Proyecto Slack Trainee
Lista de ejercicios propuestos: 

1. Cargar con datos muchos datos.
2. Listar todos los usuarios.
3. Listar todos los posts que tengan al usuario 2.
4. Listar a todos los comentarios que tengan al usuario 1.
5. Mostrar la cantidad de posteos por usuario.
6. Para un usuario en particular mostrar el segundo post que haya creado.
7. Siguiendo el ejercicio anterior listar los comentarios que le hayan hecho al post particular.
8. Listar los posteos que hayan creado solo los administradores.
9. Ordenar el ejercicio anterior de manera tal que muestre primero a los últimos posteos creados.
10. Mostrar el post que mayor cantidad de comentarios recibió.
11. Mostrar el nombre del usuario que no sea administrador y haya creado la mayor cantidad de posteos.
12. Mostrar los diferentes roles y el usuario que primero se agendó por cada rol particular.


## Ejercicio N°1
### Tabla Users
| Users | Type | Lenght 
|--|--|--|
| ID_USER | INT | 10 |
| userName | varchar | 48 |
| userMail | varchar | 48 |
| userPassword | varchar | 32 |
| ID_ROL | int | 10 |
| userDateRol | datetime |  | 

### Tabla roles
| Roles | Type | Lenght 
|--|--|--|
| ID_ROL | INT | 10 |
| rolText | varchar | 48 |
| rolDescription | varchar | 255 |

### Tabla Posts
| Post| Type | Lenght 
|--|--|--|
| ID_POST | INT | 10 |
| postDate | datetime |  |
| postContent | mediumtext |  |
| ID_USER | int | 10 |
| ID_TOPIC | int | 10 |

### Tabla Topics
| Topics | Type | Lenght 
|--|--|--|
| ID_TOPIC | INT | 10 |
| topicText | varchar | 255 |

### Tabla Comments
| Comments | Type | Lenght 
|--|--|--|
| ID_COMMENT| INT | 10 |
| commDate | datetime |  |
| commContent | varchar | 512 |
| ID_USER | int | 10 |
| ID_POST | int | 10 |


## Ejercicio N°2

    
    MariaDB [blogslack]> SELECT * FROM users
    +---------+--------------+------------------------+--------------+--------+---------------------+
    | ID_USER | userName     | userMail               | userPassword | ID_ROL | userDateRol         |
    +---------+--------------+------------------------+--------------+--------+---------------------+
    |       1 | Ablerovi     | ablerovi@email.com     | 12345678     |      3 | 2019-09-04 00:00:00 |
    |       2 | Blinkillami  | blinkillami@email.com  | 12345678     |      1 | 2019-09-03 11:27:21 |
    |       3 | Bomberferdy  | bomberferdy@email.com  | 12345678     |      2 | 2019-09-24 06:10:07 |
    |       4 | Conduper     | conduper@email.com     | 12345678     |      3 | 2019-09-14 14:45:44 |
    |       5 | Bramarcor    | bramarcor@email.com    | 87654321     |      1 | 2019-09-14 14:47:39 |
    |       6 | CrazySkunky  | crazyskunky@email.com  | 12345678     |      3 | 2019-09-12 00:00:00 |
    |       7 | DeskVirtuoso | deskvirtuoso@email.com | 12345678     |      3 | 2019-09-09 00:00:00 |
    |       8 | Devotohis    | devotohis@email.com    | 12345678     |      2 | 0000-00-00 00:00:00 |
    |       9 | Dotartamb    | dotartamb@email.com    | 12345678     |      2 | 0000-00-00 00:00:00 |
    |      10 | Featuredez   | featuredez@email.com   | 12345678     |      3 | 2019-09-14 15:01:57 |
    |      11 | Grasole      | grasole@email.com      | 12345678     |      1 | 2019-09-14 15:01:57 |
    |      12 | Greyezuear   | greyezuear@email.com   | 12345678     |      2 | 2019-09-14 15:01:57 |
    |      13 | HunterMan    | hunterman@email.com    | 12345678     |      3 | 2019-09-14 15:01:57 |
    |      14 | Issueretris  | issueretris@email.com  | 12345678     |      3 | 2019-09-14 15:01:57 |
    |      15 | KittyLink    | kittylink@email.com    | 12345678     |      1 | 2019-09-14 15:01:57 |
    +---------+--------------+------------------------+--------------+--------+---------------------+
    15 rows in set (0.00 sec)

## Ejercicio N°3

   
    MariaDB [blogslack]> SELECT ID_POST, postTitle as TITULO FROM post WHERE ID_USER=2;
    +---------+-------------+
    | ID_POST | TITULO      |
    +---------+-------------+
    |       1 | PRIMER POST |
    +---------+-------------+
    1 row in set (0.00 sec)


## Ejercicio N°4

    MariaDB [blogslack]> select * from comments where ID_USER = 1;
    +------------+---------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------+---------+
    | ID_COMMENT | commDate            | commContent                                                                                                                                                                                                                                                                                                                                                                                                      | ID_USER | ID_POST |
    +------------+---------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------+---------+
    |          1 | 2019-09-14 00:00:00 | Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eu efficitur ipsum. Proin sit amet ipsum a eros cursus pulvinar iaculis ut mi. Pellentesque vel sollicitudin ante. Vestibulum enim elit, fringilla quis aliquet et, semper vel mi. Proin lacinia lacinia sodales. In hac habitasse platea dictumst. Duis eu turpis risus. Maecenas pulvinar eros semper ex pulvinar suscipit. Nulla facilisi nullam. |       1 |       2 |
    +------------+---------------------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------+---------+
    1 row in set (0.00 sec)

## Ejercicio N°5

    MariaDB [blogslack]> SELECT post.ID_USER ID_Usuario, users.userName Nombre, COUNT(*) Comentarios FROM post JOIN users ON post.ID_USER = users.ID_USER GROUP BY post.ID_USER;
    +------------+-------------+-------------+
    | ID_Usuario | Nombre      | Comentarios |
    +------------+-------------+-------------+
    |          2 | Blinkillami |           1 |
    |          3 | Bomberferdy |           1 |
    |          5 | Bramarcor   |           3 |
    |         11 | Grasole     |           1 |
    +------------+-------------+-------------+
    4 rows in set (0.00 sec)

## Ejercicio N°6

    MariaDB [blogslack]> select ID_POST, postTitle from post where ID_USER = 5 LIMIT 1,1;
    +---------+-------------+
    | ID_POST | postTitle   |
    +---------+-------------+
    |       4 | Cuarto Post |
    +---------+-------------+
    1 row in set (0.00 sec)

## Ejercicio N°7

    MariaDB [blogslack]> SELECT comm.ID_COMMENT, comm.commDate, comm.commContent , comm.ID_POST FROM comments comm WHERE (select post.ID_POST from post where ID_USER = 5 LIMIT 1,1) = comm.ID_POST;
    +------------+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------+
    | ID_COMMENT | commDate            | commContent                                                                                                                                                                                                                                                                                                                                                                    | ID_POST |
    +------------+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------+
    |         11 | 2019-09-14 00:00:00 | Duis aute velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat |       4 |
    |         15 | 2019-09-15 00:00:00 | Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat                                                                                                                                                                                                                                |       4 |
    +------------+---------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+---------+
    2 rows in set (0.00 sec)

## Ejercicio N°8


## Ejercicio N°9


## Ejercicio N°10


## Ejercicio N°11


## Ejercicio N°12

